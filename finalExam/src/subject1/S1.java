package subject1;

class I implements Y{
    private long t;
    K k;
    public void f(){

    }
    public void setK (K k){
        this.k=k;
    }

}

class J{
    public void i(I i){

    }
}

class K{
    L l;
    M m;
    K(){
        l = new L();
    }
    public void setM (M m){
        this.m=m;
    }
}

class L{
    public void metA(){

    }
}

class M{
    public void metB(){

    }
}

class N{
    I i;
    public void setI (I i){
        this.i=i;
    }
}

interface Y{
    void f();

}

class Main{
    public static void main (String[] args){
        I i = new I();
        J j = new J();
        K k = new K();
        L l = new L();
        M m = new M();
        N n = new N();
        i.f();
        i.setK(k);
        j.i(i);
        k.setM(m);
        l.metA();
        m.metB();
        n.setI(i);

    }
}

