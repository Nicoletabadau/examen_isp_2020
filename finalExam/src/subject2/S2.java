package subject2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/*
Implement a Java GUI application (with Swing and AWT) composed of one text field,
one text area and one button. When clicking the button, the text input
in the text field is appended in the text area.
 */

class Subject2 implements ActionListener{
    JFrame fr = new JFrame();
    JButton but = new JButton("Click here!");
    JTextField tF = new JTextField();
    JTextArea tA = new JTextArea();

    public Subject2(){
        tF.setBounds(100, 100, 320, 30);
        tA.setBounds(50, 200,420 , 80);
        but.setBounds(150, 300, 220, 60);
        but.setFocusable(false);
        but.addActionListener(this);

        fr.add(but);
        fr.add(tF);
        fr.add(tA);
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setSize(550, 550);
        fr.setLayout(null);
        fr.setVisible(true);
    }



    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==but) {
            String text = tF.getText();
            tA.setText(text);
        }
    }

}

class MainClass{
    public static void main(String[] args){
        subject2.Subject2 sub2 = new subject2.Subject2();
    }
}